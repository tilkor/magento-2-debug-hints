<?php
/**
 * Copyright © 2016 Tilkor . All rights reserved.
 */
namespace Tilkor\TDebugHints\Helper;
class Data extends \Magento\Framework\App\Helper\AbstractHelper
{

	/**
     * @param \Magento\Framework\App\Helper\Context $context
     */
	public function __construct(\Magento\Framework\App\Helper\Context $context) {		
		parent::__construct($context);
	}

	public function checkHints()
	{
		$hints= $this->_getRequest()->getParam('hints');
		if($hints == 1){
			return true;
		}else{
			return false;
		}		

	}


}