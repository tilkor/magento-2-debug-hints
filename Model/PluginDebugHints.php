<?php

namespace Tilkor\TDebugHints\Model;

use Magento\Developer\Helper\Data as DevHelper;
use Magento\Developer\Model\TemplateEngine\Decorator\DebugHintsFactory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\TemplateEngineFactory;
use Magento\Framework\View\TemplateEngineInterface;
use Magento\Store\Model\ScopeInterface;
use Magento\Store\Model\StoreManagerInterface;


use Magento\Developer\Model\TemplateEngine\Plugin\DebugHints;

/**
 * Full text search implementation of autocomplete.
 */
class PluginDebugHints extends DebugHints
{
 
  /**
     * XPath of configuration of the debug block names
     */
    const XML_PATH_DEBUG_TEMPLATE_HINTS_BLOCKS = 'dev/debug/template_hints_blocks';

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var DevHelper
     */
    protected $devHelper;

    /**
     * @var DebugHintsFactory
     */
    protected $debugHintsFactory;

    /**
     * XPath of configuration of the debug hints
     *
     * Allowed values:
     *     dev/debug/template_hints_storefront
     *     dev/debug/template_hints_admin
     *
     * @var string
     */
    protected $debugHintsPath;

    /**
     * @var \Tilkor\TDebugHints\Helper\Data $helper
     */
    protected $_helper;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param DevHelper $devHelper
     * @param DebugHintsFactory $debugHintsFactory
     * @param string $debugHintsPath
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        StoreManagerInterface $storeManager,
        DevHelper $devHelper,
        DebugHintsFactory $debugHintsFactory,
        \Tilkor\TDebugHints\Helper\Data $helper,
        $debugHintsPath
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->devHelper = $devHelper;
        $this->debugHintsFactory = $debugHintsFactory;
        $this->debugHintsPath = $debugHintsPath;
        $this->_helper          = $helper;
    }

    /**
     * Wrap template engine instance with the debugging hints decorator, depending of the store configuration
     *
     * @param TemplateEngineFactory $subject
     * @param TemplateEngineInterface $invocationResult
     *
     * @return TemplateEngineInterface
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterCreate(
        TemplateEngineFactory $subject,
        TemplateEngineInterface $invocationResult
    ) {
        $storeCode = $this->storeManager->getStore()->getCode();
        if ($this->scopeConfig->getValue($this->debugHintsPath, ScopeInterface::SCOPE_STORE, $storeCode)
            && $this->devHelper->isDevAllowed()) {
            $showBlockHints = $this->scopeConfig->getValue(
                self::XML_PATH_DEBUG_TEMPLATE_HINTS_BLOCKS,
                ScopeInterface::SCOPE_STORE,
                $storeCode
            );
            return $this->debugHintsFactory->create([
                'subject' => $invocationResult,
                'showBlockHints' => $showBlockHints,
            ]);
        }

        // check ?hints=1
        $checkHints = $this->_helper->checkHints();

        if($checkHints){          
           
                if($this->checkDebugHints()){
            	
                    $showBlockHints = $this->scopeConfig->getValue(
                        self::XML_PATH_DEBUG_TEMPLATE_HINTS_BLOCKS,
                        ScopeInterface::SCOPE_STORE,
                        $storeCode
                    );

                    return $this->debugHintsFactory->create([
                        'subject' => $invocationResult,
                        'showBlockHints' => $showBlockHints,
                    ]);
                }


       
        }
           

        return $invocationResult;
    }

    public function checkDebugHints(){
        
        $storeCode = $this->storeManager->getStore()->getCode();

        $enable = $this->scopeConfig->getValue(
                    'debughints/module/is_enabled',
                    ScopeInterface::SCOPE_STORE,
                    $storeCode
                );
        

        if($enable)
        {
            
             $allowedIps = $this->scopeConfig->getValue(
                    'debughints/module/allow_ip',
                    ScopeInterface::SCOPE_STORE,
                    $storeCode
                );


             if(is_null($allowedIps)){
                return true;
            }
            else {
                $developerIp = $_SERVER['REMOTE_ADDR'];
                 
                if (strpos($allowedIps,$developerIp) !== false) {
                    return true;
                }
            }
        }
        return false;
    }



}
