Debug Hints extension helps to display path hints for template and block. It is working both for front-end and back-end without disturbing running website.

KEY FEATURES:
Magento 2 extension.
Enabled/displayed extension.
Display template/block hints in frontend.
Display template/block hints in backend.
Allow IPs.

INSTALLATION:
Copy Tilkor folder to app\code directory so that the structure should be like app/code/Tilkor/TDebugHints .
Run command "php bin/magento setup:upgrade" in Magento2 root directory.
Run command "php bin/magento setup:static-content:deploy".
Run command "chmod 777 -R pub/static var".
Go to admin panel and refresh cache.

CONFIGURATION:
Click on stores > configuration.
You can see Tilkor tab.
Click on "Tilkor" tab, then click on "Debug Hints".
Enable the extension by selecting Yes in "Enabled Template/Block Path Hints".
Leave empty for access from any location or Put 127.0.0.1 for local system or provide specific IPs in "Allowed IPs".
You can check templat/block hints both in frontend and backend by adding "?hints=1" at the end of the URL (e.g. http://www.tilkor.com/?hints=1).

SUPPORT:
For more information and support, please contact us at:
Email address: tilkor.web@gmail.com
Skyp: tilkor.web